function onSuccess(position) {
    // alert("Функция onSuccess");
    //
    // alert(JSON.stringify(position.coords));

    $('.rout-wrapper').each(function (indx, element) {
        if ($(element).hasClass('active')) {

            historyRout.push(currentRout);
        }
        $(element).removeClass('active').css({'display': 'none'});

    });
    currentRout = 'result-map';
    $('#result-map').fadeIn(fadeSpeed).addClass('active');
    initMap();
    addNewEvent(dataPush);

}

function onError(error) {
    // alert("Функция onError");
    // alert('code: ' + SON.stringify(error.code) + '\n' +
    //     'message: ' + SON.stringify(error.message) + '\n');
}

function updateLocation() {
    var test = setInterval(function () {
        navigator.geolocation.getCurrentPosition(onSuccess, onError);
    }, 30000);
}


function pushHundler(arr) {
    dataPush = arr;
    navigator.geolocation.getCurrentPosition(onSuccess, onError);
}

window.map;
window.initMap = function (lat, lng, array) {

    var myLatLng = {lat: 59.9732023, lng: 30.3403505};

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: {lat: 59.9732023, lng: 30.3403505}
    });

    var marker = new google.maps.Marker({
        position: {lat: 59.9732023, lng: 30.3403505},
        map: map,
        title: 'Hello World!'
    });
    var pos1 = new google.maps.LatLng(59.972142, 30.3406);
    var pos2 = new google.maps.LatLng(59.9726584, 30.3421237);

    var pos22 = new google.maps.Marker({
        position: pos1,
        map: map,
        title: 'Hello World!'
    });

    var pos32 = new google.maps.Marker({
        position: pos2,
        map: map,
        title: 'Hello World!'
    });

    // 59.972142
    //30.3406
    google.maps.event.trigger(map, 'resize');
};


function addNewEvent(arr) {
    // alert(arr);
}

function resizeMap() {
    if(typeof map =="undefined") return;
    setTimeout( function(){resizingMap();} , 400);
}

function resizingMap() {
    if(typeof map =="undefined") return;
    var center = map.getCenter();
    google.maps.event.trigger(map, "resize");
    map.setCenter(center);
}