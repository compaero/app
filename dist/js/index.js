document.addEventListener('deviceready', function () {
    if(JSON.parse(localStorage.getItem('auth'))) {
        $('.rout-wrapper').each(function(indx, element){
            $(element).removeClass('active').css({'display':'none'})
        });
        $('#category-filter-2').fadeIn(fadeSpeed);
        runPush();
    }
    runPush();
}, false);

var cord = {
    speed: '',
    latitude: '59.9732023',
    longitude: '30.3403505',
    accuracy: '',
    altitude: '',
    heading: '',
    altitudeAccuracy: ''
};

if (JSON.parse(localStorage.getItem('auth'))) {
    $('.rout-wrapper').each(function(indx, element){
        $(element).removeClass('active').css({'display':'none'})
    });

    $('#category-filter-2').fadeIn(fadeSpeed).addClass('active')
} else {
    $('#hello').fadeOut(fadeSpeed).removeClass('active');
    $('#login').fadeIn(fadeSpeed).addClass('active');
}
$(document).ready(function() {
    $(window).resize(function() {
        google.maps.event.trigger(map, 'resize');
    });
    google.maps.event.trigger(map, 'resize');
});