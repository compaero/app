var $preloader = $($("#preload")),
    $routeLink = $($('.rout-link')),
    fadeSpeed = 0;

window.historyRout = [];
window.currentRout;

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function route(link, parent, form) {

    historyRout.push(parent);
    if ($(form).length) {

        var formData = JSON.parse(localStorage.getItem('auth')) ? JSON.parse(localStorage.getItem('auth')) : [];

        $(form).serializeArray().map(function (item) {
            formData.push(item);
            if (item.name === "phone") {
                var code = getRandomInt(100000, 999999);
                formData.push({
                    code: code
                });
                $("#code").val(code);
            }
        });

        localStorage.setItem('auth', JSON.stringify(formData));

        $(parent).fadeOut(fadeSpeed);
        $(parent).removeClass('active');

        $preloader.fadeIn(fadeSpeed);

        setTimeout(function () {
            $preloader.fadeOut(fadeSpeed);
            $("#" + link).fadeIn(fadeSpeed);
            $("#" + link).addClass('active');
        }, getRandomInt(500, 2000))

    } else {
        $(parent).fadeOut(fadeSpeed);
        $(parent).removeClass('active');
        $("#" + link).fadeIn(fadeSpeed);
        $("#" + link).addClass('active');
    }

}

$routeLink.on('click', function (event) {
    event.preventDefault();
    currentRout = $(this).attr('data-link'); // ссылка

    var parent = $(this).closest('.rout-wrapper'), // текущая страница
        form = $(this).closest('form'); // нужна ли обработка формы

    console.log(form);
    route(currentRout, parent, form)
});

$(document).on("backbutton", function () {
    if (historyRout.length) {
        $("#" + currentRout).fadeOut(fadeSpeed);
        $(historyRout[historyRout.length - 1]).fadeIn(fadeSpeed);
        historyRout.pop([historyRout.length - 1]);
    }
});

function logOut() {
    $('.rout-wrapper').each(function (indx, element) {
        $(element).removeClass('active').css({'display': 'none'})
    });
    $('#login').fadeIn(fadeSpeed).addClass('active');
    localStorage.clear();
}


