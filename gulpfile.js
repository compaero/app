// npm instal --seve-dev gulpjs/gulp#4.0
// export PATH=./node_modules/.bin:$PATH

const Task_Config = {
    'phonegap_build': {
        'option': {
            'isRepository': true,  // Presence of a copy in the repository
            'appId': 2774101
            , // appID сan be obtained after the application is initialized build.phonegap.com
            'user': { // login and password build.phonegap.com
                'email': 'vottovaara.theband@gmail.com',
                'password': '19980911Fender'
            },
            'platforms': [ // List of required platforms
                'android'
            ],
            'download': { // The directory where the application will be downloaded
                'android': 'build/android.apk'
            },
            'hydrates': true,
            'private': true,
            'title': 'TestApp'
        },
        'keys': { // List of passwords for application keys
            'ios': {
                "password": "foobar"
            },
            'android': {
                "key_pw": "foobar",
                "keystore_pw": "foobar"
            }
        }
    }
};

// phonegap_build_my_config

var gulp = require('gulp');
var git = require('gulp-git');
var phonegapBuild = require('gulp-phonegap-build');


// {dot: true} here to inlude .pgbomit file in zip
gulp.task('phonegap-build', function () {
    gulp.src('dist/**/*', {dot: true})
        .pipe(phonegapBuild({
            'isRepository': Task_Config.phonegap_build.option.isRepository,
            'appId': Task_Config.phonegap_build.option.appId,
            'user': {
                'email': Task_Config.phonegap_build.option.user.email,
                'password': Task_Config.phonegap_build.option.user.password
            },
            'platforms': Task_Config.phonegap_build.option.platforms,
            'download': {
                'android': Task_Config.phonegap_build.option.download.android
            },
            'hydrates': false,
            'private': Task_Config.phonegap_build.option.private,
            'title': Task_Config.phonegap_build.option.title
        }));
});

gulp.task('git:add', function() {
    gulp.src(['build/*','dist/**/*.**','.gitignore','gulpfile.js','package.json'])
        .pipe(git.add())
        .pipe(git.commit('initial commit'))

});
gulp.task('git:commit', function(){
    return gulp.src('./git-test/*')
});
gulp.task('git:push', function(){
    git.push('origin', 'master', function (err) {
        if (err) throw err;
    });
});
gulp.task('git', gulp.series('git:add','git:commit','git:push'));
gulp.task('phonegap-build-debug', function () {
    gulp.src('dist/**/*', {dot: true})
        .pipe(phonegapBuild({
            'isRepository': Task_Config.phonegap_build.option.isRepository,
            'appId': Task_Config.phonegap_build.option.appId,
            'user': {
                'email': Task_Config.phonegap_build.option.user.email,
                'password': Task_Config.phonegap_build.option.user.password
            },
            'platforms': Task_Config.phonegap_build.option.platforms,
            'download': {
                'android': Task_Config.phonegap_build.option.download.android
            },
            'hydrates': Task_Config.phonegap_build.option.hydrates,
            'private': Task_Config.phonegap_build.option.private,
            'title': Task_Config.phonegap_build.option.title
        }));
});

gulp.task('default', gulp.series('phonegap-build-debug'));